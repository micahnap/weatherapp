//
//  DetailViewController.h
//  WeatherApp
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatheredSuburbs.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property WeatheredSuburbs *detailedSuburb;
@property NSString *suburbTitle;
@end

