//
//  MasterViewController.m
//  WeatherApp
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "WeatheredSuburbs.h"
#import "SuburbCell.h"
#import "AFNetworking.h"

#define jSONURL @"http://dnu5embx6omws.cloudfront.net/venues/weather.json"

@interface MasterViewController ()<UISearchBarDelegate, UISearchDisplayDelegate, UIActionSheetDelegate>

- (IBAction)btnSidePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblWeatherApp;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *weatheredSuburbs;
@property NSString *suburbTitle;
@property NSMutableArray *filteredSuburbArray;
@property UIActionSheet *actionSheet;
@property WeatheredSuburbs *detailedSuburb;
@property NSUInteger sortValue;
@property UIRefreshControl *refreshControl;

@end

@implementation MasterViewController

bool isSorting;
bool refreshing;
//block for sorting alphabetically. Referenced in different methods.
NSComparisonResult(^alphabeticSort)(id,id) = ^NSComparisonResult(id a, id b)
{
    __block NSString *first;
    __block NSString *second;
    first = [(WeatheredSuburbs*)a suburbName];
    second = [(WeatheredSuburbs*)b suburbName];
    return [first compare:second];
};

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Weather App";
    
    self.filteredSuburbArray = [NSMutableArray new];
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    
    //set up custom look for table views
    [self.tableView setBackgroundView:nil];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView registerClass:[SuburbCell class] forCellReuseIdentifier:@"Cell"];
    [self.searchDisplayController.searchResultsTableView setBackgroundView:nil];
    [self.searchDisplayController.searchResultsTableView setBackgroundColor:[UIColor clearColor]];
    [self.searchDisplayController.searchResultsTableView registerClass:[SuburbCell class] forCellReuseIdentifier:@"Cell"];
    
    //load pull to refresh
    self.refreshControl = [UIRefreshControl new];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.refreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    [self animateIntro];
    [self loadArrayForWeatheredSuburbs];
}

#pragma mark - Utility methods
- (void)refreshTableView
{
    refreshing = true;
    [self loadArrayForWeatheredSuburbs];
}

-(void)animateIntro
{
    //hide navigation bar
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //fade title in
    [UIView animateWithDuration:2.0 animations:^{
        [self.lblWeatherApp setAlpha:1.0];
    }completion:^(BOOL finished){
        //fade title out
        [UIView animateWithDuration:0.5 animations:^{
            [self.lblWeatherApp setAlpha:0.0];
        }completion:^(BOOL finished){
            //present UI
            [UIView animateWithDuration:0.5 animations:^{
                [[self navigationController] setNavigationBarHidden:NO animated:YES];
                [self.tableView setAlpha:1.0];
                [self.searchBar setAlpha:1.0];
            }completion:^(BOOL finished){
                //clean up
                self.lblWeatherApp = nil;
            }];
        }];
    }];
}

//for converting epoch seconds to timeDate strings
-(NSString *)dateStringForSeconds:(double)epochSeconds
{
    //Humanize date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy 'at' HH:mm"];
    
    NSDate *epochNSDate = [NSDate dateWithTimeIntervalSince1970:epochSeconds];

    NSString *formattedDateString = [dateFormatter stringFromDate:epochNSDate];
    
    return formattedDateString;
}

- (void)loadArrayForWeatheredSuburbs
{
    NSMutableArray *burbs = [NSMutableArray new];
    NSURL *url = [NSURL URLWithString:jSONURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    //form AFN request to return JSON response
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //on success, parse data to WeatherSuburb object and present to table view
        NSDictionary *jSonSource = (NSDictionary *)responseObject;
        NSArray* jsonArray = [jSonSource objectForKey:@"data"];
        
        [jsonArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             NSDictionary *dictWeatheredSuburbs = obj;
             
             WeatheredSuburbs *weatheredSuburb = [[WeatheredSuburbs alloc] initWithSuburbName:[dictWeatheredSuburbs objectForKey:@"_name"] andWeatherCondition:[dictWeatheredSuburbs objectForKey:@"_weatherCondition"] andPicture:[dictWeatheredSuburbs objectForKey:@"_weatherConditionIcon"] andWindInfo:[dictWeatheredSuburbs objectForKey:@"_weatherWind"] andTemperature:[dictWeatheredSuburbs objectForKey:@"_weatherTemp"] andFeelingTemp:[dictWeatheredSuburbs objectForKey:@"_weatherFeelsLike"] andUpdatedSeconds:[dictWeatheredSuburbs objectForKey:@"_weatherLastUpdated"]];
             
             NSMutableString *appendDegreeString = [weatheredSuburb.temperature mutableCopy];
             [appendDegreeString appendString:@"\u00B0"];
             weatheredSuburb.temperature = [appendDegreeString copy];
             
             //some suburbs do not have any weather relevant info, so only add the ones that do
             if (weatheredSuburb.temperature) {
                 [burbs addObject:weatheredSuburb];
             }
             
             //return empty array on fail
             NSArray* sortedArray = [burbs count] > 1 ? [burbs sortedArrayUsingComparator:alphabeticSort] : [NSArray new];
             self.weatheredSuburbs = [sortedArray copy];
             
             if (refreshing) {
                 [self.refreshControl endRefreshing];
                 refreshing = false;
             }
             
             [self.tableView reloadData];
             
         }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (refreshing) {
            [self.refreshControl endRefreshing];
            refreshing = false;
        }
        //dialog message on fail
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather Data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];

    [operation start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISearchDisplayController Delegate Methods

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    //fade out main table view on search
    [self.tableView setAlpha:0];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    //fade it back in
    [UIView animateWithDuration:1.0 animations:^{
        [self.tableView setAlpha:1];
    }];
}

//method for filtering seach content
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    //keep filtered array clear for searching
    [self.filteredSuburbArray removeAllObjects];
    
    //grab a reference to the text that the user typed in
    NSString *capitalString = [searchText uppercaseString];
    
    //look for this text within the weatheredSuburb array
    for (WeatheredSuburbs *weatheredSuburb in self.weatheredSuburbs)
    {
        //if the text is found, add it to our filtered array
        if ([weatheredSuburb.suburbName rangeOfString:capitalString options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [self.filteredSuburbArray addObject:weatheredSuburb];
        }
    }
}

//called whenever new text is entered in the search display text field.
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope: nil];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIViewController *)sender
{
    //grab reference to masters navigation controller to pass data to root view controller
    UINavigationController *navVC = segue.destinationViewController;
    DetailViewController *detailVC = [navVC.viewControllers objectAtIndex:0];
    detailVC.detailedSuburb = self.detailedSuburb;
}

#pragma mark - Table View delegate/data source methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        self.searchDisplayController.active = NO;
    }
    //deselect cell and segue
    WeatheredSuburbs *weatheredSuburb = tableView == self.searchDisplayController.searchResultsTableView ? [self.filteredSuburbArray objectAtIndex:indexPath.row] : [self.weatheredSuburbs objectAtIndex:indexPath.row];
    self.detailedSuburb = weatheredSuburb;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"detailSegue" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (tableView == self.searchDisplayController.searchResultsTableView) ? [self.filteredSuburbArray count] : [self.weatheredSuburbs count] > 1 ? [self.weatheredSuburbs count] : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    WeatheredSuburbs *weatheredSuburb;
    //tell our table view to use our custom cell
    SuburbCell *cell = (SuburbCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SuburbCell" owner:self options:nil];
    
    for (id currentObject in topLevelObjects)
    {
        if ([currentObject isKindOfClass:[UITableViewCell class]])
        {
            cell = (SuburbCell *)currentObject;
            break;
        }
    }
    
    if ([self.weatheredSuburbs count] > 1)
    {
        weatheredSuburb = (tableView == self.searchDisplayController.searchResultsTableView) ? [self.filteredSuburbArray objectAtIndex:indexPath.row] : [self.weatheredSuburbs objectAtIndex:indexPath.row];
    }
    
    //all cloudy conditions get the same image.
    if ([weatheredSuburb.picture rangeOfString:@"cloud"].location != NSNotFound || [self.weatheredSuburbs count] < 1)
    {
        weatheredSuburb.picture = @"cloud";
    }
    
    //dress the cell
    cell.lblSuburbAndTemp.text = [self.weatheredSuburbs count] > 1 ? [NSString stringWithFormat:@"%@ - %@", weatheredSuburb.suburbName, weatheredSuburb.temperature] : @"Slide down to reload";
    cell.lblLastUpdated.text = [self.weatheredSuburbs count] > 1 ? [NSString stringWithFormat:@"Last Updated: %@", [self dateStringForSeconds:[weatheredSuburb.lastUpdated doubleValue]]] : @"";
    cell.imageViewCondition.image = [UIImage imageNamed:weatheredSuburb.picture];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //makes the cells transparent for the search table view
   [cell setBackgroundColor:[UIColor clearColor]];
}

#pragma mark - UIActionsheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSArray *sortedArray;
    __block NSString *first;
    __block NSString *second;
    
    //sort arrays by various conditions
    switch (buttonIndex)
    {
            //alphabetically
        case 0:
            sortedArray = [self.weatheredSuburbs sortedArrayUsingComparator:alphabeticSort];
            self.sortValue = buttonIndex;
            break;
            
            //by temperature
        case 1:
            sortedArray = [self.weatheredSuburbs sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                first = [(WeatheredSuburbs*)a temperature];
                second = [(WeatheredSuburbs*)b temperature];
                return [first compare:second];
            }];
            self.sortValue = buttonIndex;
            break;
            
            //by last updated date
        case 2:
            sortedArray = [self.weatheredSuburbs sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                first = [(WeatheredSuburbs*)a lastUpdated];
                second = [(WeatheredSuburbs*)b lastUpdated];
                return [first compare:second];
            }];
            self.sortValue = buttonIndex;
            break;
            
            //in the event a cancellation takes place
        default:
            sortedArray = self.weatheredSuburbs;
            break;
    }
    
    self.weatheredSuburbs = sortedArray;
    [self.tableView reloadData];
    self.actionSheet = nil;
}

- (IBAction)btnSidePressed:(UIBarButtonItem *)sender
{
    NSString *alpha = @"Alphabet";
    NSString *temp = @"Temperature";
    NSString *updated = @"Last Updated";
    
    //places a bullet next to the currently selected sort
    switch (self.sortValue)
    {
        case 0:
            alpha = @"• Alphabet";
            break;
            
        case 1:
            temp = @"• Temperature";
            break;
            
        case 2:
            updated = @"• Last Updated";
            break;
    }
    
    if (!self.actionSheet)
    {
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Sort By" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:alpha,temp,updated, nil];
    }
    
    [self.actionSheet showFromBarButtonItem:sender animated:YES];
}

@end
