//
//  MasterViewController.h
//  WeatherApp
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import <UIKit/UIKit.h>


@class DetailViewController;

@interface MasterViewController : UIViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

