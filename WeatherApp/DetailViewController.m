//
//  DetailViewController.m
//  WeatherApp
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
- (IBAction)btnBackPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *lblTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lblCondition;
@property (weak, nonatomic) IBOutlet UILabel *lblFeelsLike;
@property (weak, nonatomic) IBOutlet UILabel *lblWindSpeed;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSwipe;

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //add swipe right gesture to screen. allows user to swipe to go back
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    [self setLabels];
}

-(void)viewDidAppear:(BOOL)animated
{
    //inform user of swipe capabilities first time only for the life of the bundle.
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    bool notFirstRodeo = [userDefaults boolForKey:@"notFirstRodeo"];
    
    if (!notFirstRodeo) {
        [UIView animateWithDuration:2.0 animations:^{
            [self.imageViewSwipe setAlpha:1.0];
        }completion:^(BOOL finished){
            [UIView animateWithDuration:1.0 animations:^{
                [self.imageViewSwipe setAlpha:0.0];
            }];
        }];
        
        notFirstRodeo = !notFirstRodeo;
        [userDefaults setBool:notFirstRodeo forKey:@"notFirstRodeo"];
        [userDefaults synchronize];
    }
}

-(void)setLabels
{
    self.title = self.detailedSuburb.suburbName;
    self.imageView.image = [UIImage imageNamed:self.detailedSuburb.picture];
    self.lblTemperature.text = self.detailedSuburb.temperature;
    self.lblCondition.text = self.detailedSuburb.weatherCondition ? self.detailedSuburb.weatherCondition : @"Unknown Condition";
    
    //extract windspeed data from wind string and set
    NSString *rangedString = [self.detailedSuburb.wind componentsSeparatedByString:@" "][3];
    self.lblWindSpeed.text = [NSString stringWithFormat:@"Wind Speed: %@", rangedString];
    
    //Add degree string to feels like temp and set
    NSMutableString *degreeString = [self.detailedSuburb.feelingTemp mutableCopy];
    [degreeString appendString:@"\u00B0"];
    self.lblFeelsLike.text = [NSString stringWithFormat:@"Feels like %@", degreeString];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackPressed:(id)sender
{
    UINavigationController *nav = [self.splitViewController.viewControllers objectAtIndex:0];
    [nav popViewControllerAnimated:YES];
}

- (void)swipeDetected:(UISwipeGestureRecognizer *)sender
{
    [self btnBackPressed:self];
}
@end
