//
//  SuburbCell.h
//  WeatherApp
//
//  Created by Micah Napier on 31/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuburbCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewCondition;
@property (weak, nonatomic) IBOutlet UILabel *lblSuburbAndTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdated;

@end
