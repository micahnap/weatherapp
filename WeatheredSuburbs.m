//
//  WeatheredSuburbs.m
//  WeatherApp
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import "WeatheredSuburbs.h"

@implementation WeatheredSuburbs

-(id)initWithSuburbName:(NSString *)suburbName andWeatherCondition:(NSString *)weatherCondition andPicture:(NSString *)picture andWindInfo:(NSString *)windInfo andTemperature:(NSString *)temperature andFeelingTemp:(NSString *)feelsLikeTemp andUpdatedSeconds:(NSString *)lastUpdatedSeconds
{
    if (self)
    {
        self.suburbName = suburbName;
        self.weatherCondition = weatherCondition;
        self.wind = windInfo;
        self.picture = picture;
        self.temperature = temperature;
        self.feelingTemp = feelsLikeTemp;
        self.lastUpdated = lastUpdatedSeconds;
    }
    
    return self;
}

@end
