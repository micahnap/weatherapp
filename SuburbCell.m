//
//  SuburbCell.m
//  WeatherApp
//
//  Created by Micah Napier on 31/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import "SuburbCell.h"

@implementation SuburbCell

@synthesize imageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
