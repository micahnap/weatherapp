//
//  WeatheredSuburbs.h
//  WeatherApp
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 micah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatheredSuburbs : NSObject

@property NSString *suburbName;
@property NSString *weatherCondition;
@property NSString *picture;
@property NSString *temperature;
@property NSString *feelingTemp;
@property NSString *wind;
@property NSString *lastUpdated;


-(id)initWithSuburbName:(NSString *)suburbName andWeatherCondition:(NSString *)weatherCondition andPicture:(NSString *)picture andWindInfo:(NSString *)windInfo andTemperature:(NSString *)temperature andFeelingTemp:(NSString *)feelsLikeTemp andUpdatedSeconds:(NSString *)lastUpdatedSeconds;

@end
